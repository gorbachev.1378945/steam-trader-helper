export default function Utils() {}

Utils.prototype.getCookie = async function(name, domain = null) {
	let params = {name:name};
	if(domain != null)
		params.domain = domain;

	return new Promise((resolve, reject) => {
		chrome.cookies.getAll(params, function(cookies){
	    	resolve(cookies[0]);
	    });
	});
};

Utils.prototype.urlencodeFormData = function(fd){
    let s = '';
    function encode(s){ return encodeURIComponent(s).replace(/%20/g,'+'); }
    for(let pair of fd.entries()){
        if(typeof pair[1]=='string'){
            s += (s?'&':'') + encode(pair[0])+'='+encode(pair[1]);
        }
    }
    return s;
};

async function getActiveTab() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            resolve(tabs[0]);
        });
    });
};

Utils.prototype.sendAlertToUserUi = async function(text) {
    let tab = await getActiveTab();
    chrome.tabs.sendMessage(tab.id, {"action":"alert","text":text});
}